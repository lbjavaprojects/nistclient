import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class TimeClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		try(Socket s=new Socket("localhost", 8189);
				InputStream is=s.getInputStream();
				Scanner sc=new Scanner(is)){
			while(sc.hasNextLine()){
				System.out.println(sc.nextLine());
			}
		}
	}

}
